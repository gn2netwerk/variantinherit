<?php

class GN2_VariantInherit_Details extends GN2_VariantInherit_Details_Parent {

    protected function _prepareMetaDescription( $sMeta, $iLength = 200, $blDescTag = false )
    {
        if ( !$sMeta ) {
            $oProduct = $this->getProduct();
            $oParentID = $oProduct->oxarticles__oxparentid->value;
            $actLang = oxRegistry::getLang()->getBaseLanguage();


            if($oParentID != ""){
                // Falls Vaterprodukt vorhanden, lese Seo-Text aus

                $oDb = oxDb::getDb();
                $sSelect = 'SELECT OXDESCRIPTION FROM oxobject2seodata WHERE OXOBJECTID = "'.mysql_real_escape_string($oParentID).'" AND OXLANG = "'.mysql_real_escape_string($actLang).'"';
                $oDesc = $oDb->getOne($sSelect);

                if($oDesc != ""){
                    $sMeta = $oDesc;
                    return parent::_prepareMetaDescription( $sMeta, $iLength, $blDescTag );
                }

            }


            // OXID-Standard: Langbeschreibung als Meta-Text verwenden
            $sMeta = $oProduct->getLongDescription()->value;
            $sMeta = str_replace( array( '<br>', '<br />', '<br/>' ), "\n", $sMeta );
            $sMeta = $oProduct->oxarticles__oxtitle->value.' - '.$sMeta;
            $sMeta = strip_tags( $sMeta );

        }
        return parent::_prepareMetaDescription( $sMeta, $iLength, $blDescTag );
    }

}