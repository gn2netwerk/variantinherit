<?php

class GN2_VariantInherit_OxArticle extends GN2_VariantInherit_OxArticle_Parent {

    /**
     * Loads and returns array with crosselling information.
     *
     * @return array
     */
    function getCrossSelling()
    {
        $oCrosslist = oxNew( "oxarticlelist");
        $oCrosslist->loadArticleCrossSell($this->oxarticles__oxid->value);
        if ( $oCrosslist->count() ) {
            return $oCrosslist;
        }

        // GN2 - Check Parent for CrossSelling
        if($this->oxarticles__oxparentid->value != ""){
            $oCrosslist = oxNew( "oxarticlelist");
            $oCrosslist->loadArticleCrossSell($this->oxarticles__oxparentid->value);
            if ( $oCrosslist->count() ) {
                return $oCrosslist;
            }
        }

    }


    /**
     * Loads and returns array with accessoires information.
     *
     * @return array
     */
    function getAccessoires()
    {
        $myConfig = $this->getConfig();

        // Performance
        if ( !$myConfig->getConfigParam( 'bl_perfLoadAccessoires' ) ) {
            return;
        }

        $oAcclist = oxNew( "oxarticlelist");
        $oAcclist->setSqlLimit( 0, $myConfig->getConfigParam( 'iNrofCrossellArticles' ));
        $oAcclist->loadArticleAccessoires($this->oxarticles__oxid->value);

        if ( $oAcclist->count()) {
            return $oAcclist;
        }

        // GN2 - Check Parent for Accessoires
        if($this->oxarticles__oxparentid->value != ""){
            $oAcclist = oxNew( "oxarticlelist");
            $oAcclist->setSqlLimit( 0, $myConfig->getConfigParam( 'iNrofCrossellArticles' ));
            $oAcclist->loadArticleAccessoires($this->oxarticles__oxparentid->value);

            if ( $oAcclist->count()) {
                return $oAcclist;
            }
        }

    }

}