<?php

/**
 * Module information
 */

$aModule = array(
    'id'            => 'gn2_variantinherit',
    'title'         => 'gn2 :: VariantInherit',
    'description'   => 'Behebt Vererbungs-Probleme von diversen Artikel-Informationen.<br>
                        Vererbt Infos von Vaterartikel auf Varianten, wenn die entsprechenden Infos bei Varianten nicht eingetragen sind:<br>
                        - CrossSelling & Zubeh&ouml;r<br>
                        - Attribute (Merkmale)<br>
                        - Meta-Descriptions
    ',
    'thumbnail'     => 'gn2.jpg',
    'version'       => '0.1',
    'author'        => 'GN2 netwerk',
    'extend'        => array(
        'oxarticle' => 'gn2netwerk/variantinherit/gn2_variantinherit_oxarticle',
        'details'   => 'gn2netwerk/variantinherit/gn2_variantinherit_details',
    )
);